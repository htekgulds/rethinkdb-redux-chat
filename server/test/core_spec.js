/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */
import {expect} from "chai";
import {Map, fromJS} from 'immutable';

import {setMessages, addMessage} from '../src/core';

describe('application logic', () => {

    describe('setMessages', () => {

        it('should add messages to the state', () => {
            let today = new Date();
            const state = Map();
            const messages = fromJS([
                { message: "Hello!", date: today },
                { message: "Hi!", date: today }
            ]);
            const nextState = setMessages(state, messages);
            expect(nextState).to.equal(fromJS({
                messages: [
                    { message: "Hello!", date: today },
                    { message: "Hi!", date: today }
                ]
            }));
        });

        it('should convert to immutable', () => {
            let today = new Date();
            const state = Map();
            const messages = [
                { message: "Hello!", date: today },
                { message: "Hi!", date: today }
            ];
            const nextState = setMessages(state, messages);
            expect(nextState).to.equal(fromJS({
                messages: [
                    { message: "Hello!", date: today },
                    { message: "Hi!", date: today }
                ]
            }));
        });
    });
    
    describe('addMessage', () => {
        
        it('should add a message to the state', () => {
            let today = new Date();
            const state = fromJS({
                messages: [
                    { message: "Hello!", date: today }
                ]
            });
            const message = Map({ message: "Hi!", date: today });
            const nextState = addMessage(state, message);
            expect(nextState).to.equal(fromJS({
                messages: [
                    { message: "Hello!", date: today },
                    { message: "Hi!", date: today }
                ]
            }));
        });
        
        it('should convert to immutable', () => {
            let today = new Date();
            const state = fromJS({
                messages: [
                    { message: "Hello!", date: today }
                ]
            });
            const message = { message: "Hi!", date: today };
            const nextState = addMessage(state, message);
            expect(nextState).to.equal(fromJS({
                messages: [
                    { message: "Hello!", date: today },
                    { message: "Hi!", date: today }
                ]
            }));
        });
    });
});