/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */

import {expect} from 'chai';
import {Map, fromJS} from 'immutable';

import reducer from '../src/reducer';

describe('reducer', () => {

    it('should handle SET_MESSAGES', () => {
        let today = new Date();
        const initialState = Map();
        const action = {type: 'SET_MESSAGES', messages: fromJS([
            { message: "Hello!", date: today },
            { message: "Hi!", date: today }
        ])};
        const nextState = reducer(initialState, action);
        expect(nextState).to.equal(fromJS({
            messages: [
                { message: "Hello!", date: today },
                { message: "Hi!", date: today }
            ]
        }));
    });

    it('should handle ADD_MESSAGE', () => {
        let today = new Date();
        const initialState = fromJS({
            messages: [
                { message: "Hello!", date: today }
            ]
        });
        const action = {type: 'ADD_MESSAGE', message: Map({ message: "Hi!", date: today })};
        const nextState = reducer(initialState, action);
        expect(nextState).to.equal(fromJS({
            messages: [
                { message: "Hello!", date: today },
                { message: "Hi!", date: today }
            ]
        }));
    });

    it('should have an initial state', () => {
        let today = new Date();
        const action = {type: 'ADD_MESSAGE', message: Map({ message: "Hi!", date: today })};
        const nextState = reducer(undefined, action);
        expect(nextState).to.equal(fromJS({
            messages: [
                { message: "Hi!", date: today }
            ]
        }));
    });

    it('should not handle for invalid action', () => {
        let today = new Date();
        const initialState = fromJS({
            messages: [
                { message: "Hello!", date: today }
            ]
        });
        const action = {type: 'SOMETHING_ELSE', message: Map({ message: "Hi!", date: today })};
        const nextState = reducer(initialState, action);
        expect(nextState).to.equal(fromJS({
            messages: [
                { message: "Hello!", date: today }
            ]
        }));
    });
});