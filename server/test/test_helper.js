/**
 * Created by Hasan TEKGÜL on 31.05.2016.
 */
import chai from 'chai';
import chaiImmutable from 'chai-immutable';

chai.use(chaiImmutable);