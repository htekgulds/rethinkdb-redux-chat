/**
 * Created by Hasan TEKGÜL on 31.05.2016.
 */
import express from 'express';
import http from 'http';
import socket from 'socket.io';
import rethinkdb from 'rethinkdbdash';
import async from 'async';
import bodyParser from 'body-parser';
import {createStore} from 'redux';

import routes from "./src/routes";
import jobs from './src/async-jobs';
import reducer from './src/reducer';

let app = express();
let server = http.createServer(app);
let io = socket(server);
let RDB_HOST = process.env.CI ? 'rethinkdb' : '192.168.99.100';
let r = rethinkdb({host: RDB_HOST});

const store = createStore(reducer);
store.subscribe(() => {
    console.log('State changed!...', store.getState());
    io.emit('state', store.getState().toJS());
});

async.waterfall(jobs(r), err => {
    if(err) throw err;
    console.log('Async Job finished... Listening to new messages');
    r.db('redux_chat')
        .table('messages')
        .changes()
        .run()
        .then(cursor => {
            cursor.each((err, result) => {
                if (!!err) throw err;

                console.log(result);
                // io.emit('new_message', result);
            });
        });
    store.dispatch({
        type: 'SET_MESSAGES',
        messages: [{id: 1, message: "Hello World!", date: new Date()}, {id: 2, message: "heeey!", date: new Date()}]
    });
});

app.set('r', r);
app.set('router', express.Router());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use('/message', routes(app));

io.on('connection', socket => {
    socket.emit('state', store.getState().toJS());
    socket.on('action', store.dispatch.bind(store));
    console.log('New client connected!')
});

var PORT = 3000;
server.listen(PORT);
console.log('RethinkDB Chat App running on ' + PORT);