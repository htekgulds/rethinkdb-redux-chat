/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */
import {List, fromJS} from "immutable";

export const setMessages = (state, messages) => {
    return state.set('messages', fromJS(messages));
};

export const addMessage = (state, message) => {
    const messages = state.get('messages', List()).push(fromJS(message));
    return state.set('messages', messages);
};