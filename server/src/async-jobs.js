const jobs = (r) => {
    
    const dbExists = (finished) =>
        r.dbList()
            .contains('redux_chat')
            .run()
            .then(result => finished(null, result))
            .error(err => finished(err));

    const createDb = (dbExists, finished) => {
        if (dbExists == false) {
            r.dbCreate('redux_chat')
                .run()
                .then(() => finished(null))
                .error(err => finished(err));
        }
        else {
            finished(null)
        }
    };

    const tableExists = (finished) =>
        r.db('redux_chat')
            .tableList()
            .contains('messages')
            .run()
            .then(result => finished(null, result))
            .error(err => finished(err));

    const createTable = (tableExists, finished) => {
        if (tableExists == false) {
            r.db('redux_chat')
                .tableCreate('messages')
                .run()
                .then(result => finished(null, false))
                .error(err => finished(err));
        }
        else {
            finished(null, true);
        }
    };

    const createIndex = (tableIndexed, finished) => {
        if (tableIndexed == false) {
            r.db('redux_chat')
                .table('messages')
                .indexCreate('date')
                .run()
                .then(result => finished(null))
                .error(err => finished(err));
        }
        else {
            finished(null);
        }
    };
    
    return [
        dbExists,
        createDb,
        tableExists,
        createTable,
        createIndex
    ];
};

export default jobs;