/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */
import {Map} from "immutable";
import {setMessages, addMessage} from "./core";

const reducer = (state = Map(), action) => {
    switch (action.type) {
        case 'SET_MESSAGES':
            return setMessages(state, action.messages);
        case 'ADD_MESSAGE':
            return addMessage(state, action.message);
    }
    return state;
};

export default reducer;