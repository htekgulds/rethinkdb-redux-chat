/**
 * Created by Hasan TEKGÜL on 31.05.2016.
 */

const routes = (app) => {
    let r = app.get('r');
    let router = app.get('router');

    router.post('/send', (req, res) => {
        let message = req.body.message;
        r.db('redux_chat').table('messages')
            .insert({
                message: message,
                date: new Date()
            })
            .run()
            .then(result => res.send('Message sent!'))
            .error(err => res.status(500).send(err));
    });

    router.get('/all', (req, res) => {
        r.db('redux_chat').table('messages')
            .orderBy({index: r.desc('date')})
            .run()
            .then(result => res.send(result))
            .error(err => res.status(500).send(err));
    });

    return router;
};

export default routes;