/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */

import {expect} from 'chai';
import {Map, fromJS} from 'immutable';

import reducer from '../src/reducer';

describe('redcuer', () => {
    
    it('should handle SET_STATE', () => {
        let today = new Date();
        const initialState = Map();
        const action = {type: 'SET_STATE', state: fromJS({
            messages: [
                { message: "Hello World!", date: today }
            ]
        })};
        const nextState = reducer(initialState, action);
        expect(nextState).to.equal(fromJS({
            messages: [
                { message: "Hello World!", date: today }
            ]
        }));
    });

    it('should handle SEND_MESSAGE', () => {
        let today = new Date();
        const initialState = fromJS({
            messages: [
                { message: "Hello World!", date: today }
            ]
        });
        const action = {type: 'SEND_MESSAGE', message: "Whats going on..."};
        const nextState = reducer(initialState, action);
        expect(nextState).to.equal(fromJS({
            messages: [
                { message: "Hello World!", date: today },
                { message: "Whats going on...", date: today }
            ]
        }));
    });

    it('should have an initial state', () => {
        let today = new Date();
        const action = {type: 'SET_STATE', state: fromJS({
            messages: [
                { message: "Hello World!", date: today }
            ]
        })};
        const nextState = reducer(undefined, action);
        expect(nextState).to.equal(fromJS({
            messages: [
                { message: "Hello World!", date: today }
            ]
        }));
    });

    it('should not handle for invalid action', () => {
        let today = new Date();
        const initialState = Map();
        const action = {type: 'SOMETHING_ELSE', state: fromJS({
            messages: [
                { message: "Hello World!", date: today }
            ]
        })};
        const nextState = reducer(initialState, action);
        expect(nextState).to.equal(Map());
    });
});