/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */
var webpack = require('webpack');

module.exports = {

    entry: [
        'webpack-dev-server/client?http://localhost:8080',
        'webpack/hot/only-dev-server',
        './src/index.jsx'
    ],
    output: {
        path: __dirname + '/dist',
        publicPath: '/',
        filename: 'bundle.js'
    },

    devServer: {
        contentBase: './dist',
        hot: true,
        colors: true
    },

    resolve: {
        extensions: ['', '.js', '.jsx']
    },

    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: 'style!' + 'css?sourceMap'
            },
            {
                test: /\.scss$/,
                loader: 'style!' + 'css?sourceMap' + '!sass?sourceMap'
            },
            {
                // JS / JSX files go here
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'react-hot!' + 'babel-loader'
            },
            {
                // JSON files go here
                test: /\.(json)$/,
                exclude: /node_modules/,
                loader: 'json-loader'
            },
            {
                // Font files go here
                test: /\.(svg|ttf|woff|woff2|eot)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader'
            }
        ]
    },
    devtool: 'source-map',

    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
};