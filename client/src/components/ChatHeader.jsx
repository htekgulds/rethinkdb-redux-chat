/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */
import React from "react";

export default class ChatHeader extends React.Component {
    render() {
        return (
            <h1>{this.props.title}</h1>
        );
    }
}