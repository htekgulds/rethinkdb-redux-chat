/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */
import React from 'react';

export default class MessageItem extends React.Component {
    render() {
        return (
            <li>
                <strong>{this.props.message.message}</strong> : {this.props.message.date}
            </li>
        );
    }
}