/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */
import React from 'react';
import {connect} from 'react-redux';
import MessageItem from './MessageItem';

const mapStateToProps = (state) => ({
    messages: state.get('messages') ? state.get('messages').toJS() : []
});

export default class MessageList extends React.Component {
    render() {
        return (
            <ul>
                {this.props.messages.map(message => <MessageItem key={message.id} message={message} />)}
            </ul>
        );
    }
}

export const MessageListContainer = connect(mapStateToProps)(MessageList);