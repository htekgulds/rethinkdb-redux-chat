/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */
import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap';

import ChatHeader from './ChatHeader';
import {MessageListContainer} from './MessageList';
import {MessageBoxContainer} from './MessageBox';

const App = () => {
    return (
        <div className="main">
            <Grid>
                <Row>
                    <Col md={12} >
                        <ChatHeader title="Redux Chat App"/>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <MessageListContainer />
                    </Col>
                </Row>
                <Row>
                    <Col md={4}>
                        <MessageBoxContainer />
                    </Col>
                </Row>
            </Grid>
        </div>
    );
};

export default App;