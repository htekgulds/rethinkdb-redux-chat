/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */
import React from 'react';
import DOM from 'react-dom';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {FormGroup, InputGroup, FormControl, Button} from 'react-bootstrap';
import {connect} from 'react-redux';

const mapDispatchToProps = dispatch => ({
    onMessageSend: (message) => dispatch({type: 'SEND_MESSAGE', message: message})
});

export default class MessageBox extends React.Component {
    constructor() {
        super();
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    }
    render() {
        return (
            <FormGroup>
                <InputGroup>
                    <FormControl ref="input" type="text" placeholder="Type your message here..."/>
                    <InputGroup.Button>
                        <Button bsStyle="primary" onClick={this.onMessageSend}>SEND</Button>
                    </InputGroup.Button>
                </InputGroup>
            </FormGroup>
        );
    }    
    onMessageSend() {
        let message = DOM.findDOMNode(this.refs.input).value;
        this.props.onMessageSend(message);
    }
}

export const MessageBoxContainer = connect(null, mapDispatchToProps)(MessageBox);