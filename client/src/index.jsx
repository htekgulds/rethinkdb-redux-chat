/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */
require('bootstrap/dist/css/bootstrap.min.css');

import React from 'react';
import DOM from 'react-dom';
import io from 'socket.io-client';
import {createStore} from 'redux';
import {Provider} from 'react-redux';

import App from './components/App';
import reducer from './reducer';
const store = createStore(reducer);

const socket = io(`${location.protocol}//${location.hostname}:3000`);
socket.on('state', state => store.dispatch({type: 'SET_STATE', state}));

const run = () => {
    DOM.render(
        <Provider store={store}>
            <App />
        </Provider>,
        document.getElementById('app')
    );
};
store.subscribe(run);