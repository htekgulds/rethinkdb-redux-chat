/**
 * Created by Hasan TEKGÜL
 * on 1.06.2016.
 */
import {Map, List} from 'immutable';

const reducer = (state = Map(), action) => {
    switch (action.type) {
        case 'SET_STATE':
            return state.merge(action.state);
        case 'SEND_MESSAGE':
            let messages = state.get('messages', List()).push(action.message);
            return state.set('messages', messages);
    }
    return state;
};

export default reducer;