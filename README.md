### This is a sample Chat App to try out the folowing tech:

 * RethinkDB (NoSQL DB with realtime push capabilities)
 * React (Frontend view framework)
 * Redux (Immutable state library)
 * NodeJS & Express (For server side api)
 * Socket.IO (Websockets for reltime server-client communication)